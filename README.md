# README #

The repository contains a simple script to get partners data from [Kiva](https://kiva.org).
You can open the output files in Excel or upload them to MySQL (or some other database).

I have deliberately kept the column names as close to the names in the API as possible

### How do I get set up? ###

* Clone the repo
* Execute partners.php file in the scripts folder
* All the output CSV files will be created in the data folder

### Who do I talk to? ###

* Get in touch in case of any errors or if you want me to add more to the repo

### What about the loans and lenders data? ###

* Working on it :)

### TODOs ###

* Set up a cron job to update the data every 24 hours on [data.world](https://data.world)
* Set up a cron job to update the data every 24 hours on [Google BigQuery](https://cloud.google.com/bigquery)
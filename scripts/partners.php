<?php

    /*
        This script is to use Kiva API and get all partners data. Save the output of this
        file in s3, data.world, google bigquery etc
    */

    error_reporting(E_ALL);
    ini_set('display_errors', TRUE);

    require("partners.class.php");

    define("PARTNERS_API", "https://api.kivaws.org/v1/partners.json");

    // names of the output data files
    define("PARTNERS_DATA", "../data/partners.csv");
    define("PARTNER_COUNTRIES_DATA", "../data/partner_countries.csv");
    define("PARTNERS_SOCIAL_PERFORMANCE_STRENGTHS_DATA", "../data/partner_social_performance_strengths.csv");
    define("PARTNERS_DATA_FILE_DATA", "../data/partners.csv");
    define("SOCIAL_PERFORMANCE_STRENGTHS_DATA", "../data/social_performance_strengths.csv");
    define("COUNTRIES_DATA", "../data/countries.csv");


    $partners = new partners();
    $partners->process();


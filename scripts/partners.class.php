<?php

    class partners {
            private $partners_file;
            private $partner_countries_file;
            private $partner_social_performance_strengths;
            private $locations;
            private $social_performance_strengths;

        public function __construct() {
            $this->partners_file = fopen(PARTNERS_DATA, "w");
            $this->partner_countries_file = fopen(PARTNER_COUNTRIES_DATA, "w");
            $this->partner_social_performance_strengths_file = fopen(PARTNERS_SOCIAL_PERFORMANCE_STRENGTHS_DATA, "w");
            $this->locations = [];
            $this->social_performance_strengths = [];
        }

        /*
            Function to print headers of the various CSV files - partners, social performance strengths etc
        */
        private function print_headers() {
            $partners_file_headers = ['id', 'name', 'status', 'rating', 'image_id', 'image_template_id',
                            'start_date', 'delinquency_rate', 'default_rate', 'total_amount_raised', 
                            'loans_posted', 'delinquency_rate_note', 'default_rate_note', 'portfolio_yield_note',
                            'charges_fees_and_interest', 'average_loan_size_percent_per_capita_income', 
                            'loans_at_risk_rate', 'currency_exchange_loss_rate', 'url'];
            fputcsv($this->partners_file, $partners_file_headers);

            $partner_countries_file_headers = ['partner_id', 'iso_code'];
            fputcsv($this->partner_countries_file, $partner_countries_file_headers);

            $partner_social_performance_streght_headers = ['partner_id', 'social_performance_strength_id'];
            fputcsv($this->partner_social_performance_strengths_file, $partner_social_performance_streght_headers);
        }

        private function save_social_performance_strengths() {
            $headers = ['id', 'name', 'description'];
            $sps_file = fopen(SOCIAL_PERFORMANCE_STRENGTHS_DATA, "w");
            fputcsv($sps_file, $headers);
            if(isset($this->social_performance_strengths) && is_array($this->social_performance_strengths)
                        && count($this->social_performance_strengths) > 0) {
                foreach($this->social_performance_strengths as $sps) {
                    fputcsv($sps_file, $sps);
                }
            }
            fclose($sps_file);
        }

        public function process() {
            $this->print_headers();

            $current_page = 1;
            $total_pages = 1;              

            while($current_page <= $total_pages) {
                $current_url = PARTNERS_API . "?page=" . $current_page;
                $data = $this->get_curl_data($current_url);

                if (isset($data['paging']) && is_array($data['paging']) && count($data['paging']) > 0) {
                    $paging = $data['paging'];
                    $total_pages = (isset($paging['pages'])) ? $paging['pages'] : $total_pages;
                }

                if (isset($data['partners']) && is_array($data['partners']) && count($data['partners']) > 0) {
                    $partners = $data['partners'];
                    $this->save_partners($partners);
                }        

                $current_page ++;
            }

            $this->save_locations();
            $this->save_social_performance_strengths();

            fclose($this->partners_file);
            fclose($this->partner_countries_file);
            fclose($this->partner_social_performance_strengths_file);           
        }

        // use curl to get partners data and return partners array
        private function get_curl_data($url) {
            $curl = curl_init(); 
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $json_output = curl_exec($curl);
            if(curl_error($curl)) {
                die("Error during curl call : " . curl_error($curl));
            }
            curl_close($curl);

            $data = json_decode($json_output, TRUE);
            return $data;
        }

        private function save_locations() {
            $locations_file = fopen(COUNTRIES_DATA, "w");
            $headers = ['iso_code', 'region', 'name', 'geo_level', 'geo_latitude', 'geo_longitude', 'geo_type'];
            fputcsv($locations_file, $headers);
            $locations = $this->locations;
            if(isset($locations) && is_array($locations) && count($locations) > 0) {
                foreach($locations as $l) {
                    fputcsv($locations_file, $l);
                }
            }
            fclose($locations_file);
        }


        function save_partners($partners) {
            if (is_array($partners) && count($partners) > 0) {
                foreach ($partners as $p) {
                    $partner = [];

                    $partner[] = $partner_id = isset($p['id']) ? $p['id'] : "";
                    $partner[] = isset($p['name']) ? $p['name'] : "";
                    $partner[] = isset($p['status']) ? $p['status'] : "";
                    $partner[] = isset($p['rating']) ? $p['rating'] : "";
                    $partner[] = isset($p['image']['id']) ? $p['image']['id'] : "";
                    $partner[] = isset($p['image']['template_id']) ? $p['image']['template_id'] : "";
                    $partner[] = isset($p['start_date']) ? $p['start_date'] : "";
                    $partner[] = isset($p['delinquency_rate']) ? $p['delinquency_rate'] : "";
                    $partner[] = isset($p['default_rate']) ? $p['default_rate'] : "";
                    $partner[] = isset($p['total_amount_raised']) ? $p['total_amount_raised'] : "";
                    $partner[] = isset($p['loans_posted']) ? $p['loans_posted'] : "";
                    $partner[] = isset($p['delinquency_rate_note']) ? $p['delinquency_rate_note'] : "";
                    $partner[] = isset($p['default_rate_note']) ? $p['default_rate_note'] : "";
                    $partner[] = isset($p['portfolio_yield_note']) ? $p['portfolio_yield_note'] : "";
                    $partner[] = isset($p['charges_fees_and_interest']) ? $p['charges_fees_and_interest'] : "";
                    $partner[] = isset($p['average_loan_size_percent_per_capita_income']) ? 
                                    $p['average_loan_size_percent_per_capita_income'] : "";
                    $partner[] = isset($p['loans_at_risk_rate']) ? $p['loans_at_risk_rate'] : "";
                    $partner[] = isset($p['currency_exchange_loss_rate']) ? $p['currency_exchange_loss_rate'] : "";
                    $partner[] = isset($p['url']) ? $p['url'] : "";

                    $countries = [];
                    $country_iso_codes = [];
                    if (isset($p['countries']) && is_array($p['countries']) && count($p['countries']) > 0) {
                        foreach($p['countries'] as $c) {
                            $country = [];
                            $country[]  = $iso_code = isset($c['iso_code']) ? $c['iso_code'] : "";
                            if (!isset($this->locations[$iso_code])) {
                                $country[]  = isset($c['region']) ? $c['region'] : "";
                                $country[]  = isset($c['name']) ? $c['name'] : "";
                                $country[]  = isset($c['location']['geo']['level']) ? $c['location']['geo']['level'] : "";
                                $pairs      = isset($c['location']['geo']['pairs']) ? $c['location']['geo']['pairs'] : "";
                                $pairs      = explode(" ", trim($pairs));
                                $country[]  = $pairs[0];
                                $country[]  = $pairs[1];
                                $country[]  = isset($c['location']['geo']['type']) ? $c['location']['geo']['type'] : "";

                                $this->locations[$iso_code] = $country;
                            }
                            
                            fputcsv($this->partner_countries_file, array($partner_id, $iso_code));
                        }
                    }                

                    $partner[] = implode(",", $countries);
                    $partner[] = implode(",", $country_iso_codes);

                    if (isset($p['social_performance_strengths']) && is_array($p['social_performance_strengths']) && 
                            count($p['social_performance_strengths']) > 0) {
                                foreach($p['social_performance_strengths'] as $sps) {
                                    $sps_id = $sps['id'];
                                    if(!isset($this->social_performance_strengths[$sps_id])) {
                                        $this->social_performance_strengths[$sps_id] = 
                                            array(
                                                    $sps['id'], 
                                                    $sps['name'], 
                                                    $sps['description']
                                            );
                                    }
                                    fputcsv($this->partner_social_performance_strengths_file, array($partner_id, $sps_id));
                                }
                            }

                    fputcsv($this->partners_file, $partner);
                }
            }
        }
    }
